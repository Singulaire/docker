FROM gcr.io/tensorflow/tensorflow:0.8.0-devel-gpu
MAINTAINER Brody Huval <brodyh@stanford.edu>

#### From nvidia-docker/ubuntu-14.04/cuda/7.5/Dockerfile ####
RUN apt-get update && apt-get install -y wget

RUN wget -q -O - http://developer.download.nvidia.com/compute/cuda/repos/GPGKEY | apt-key add - && \
    echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1404/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
    apt-get update

ENV CUDA_VERSION 7.5
LABEL com.nvidia.cuda.version="7.5"

RUN apt-get install -y --no-install-recommends --force-yes "cuda-toolkit-7.5"

RUN echo "/usr/local/cuda/lib" >> /etc/ld.so.conf.d/cuda.conf && \
    echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
    ldconfig

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/cuda/lib:/usr/local/cuda/lib64:${LD_LIBRARY_PATH}

#### Install cudnn v2 ####
COPY cudnn/cudnn-6.5-linux-x64-v2/libcudnn.so /usr/local/cuda/lib64/libcudnn.so
COPY cudnn/cudnn-6.5-linux-x64-v2/libcudnn.so.6.5 /usr/local/cuda/lib64/libcudnn.so.6.5
COPY cudnn/cudnn-6.5-linux-x64-v2/libcudnn.so.6.5.48 /usr/local/cuda/lib64/libcudnn.so.6.5.48
COPY cudnn/cudnn-6.5-linux-x64-v2/cudnn.h /usr/local/cuda/include/cudnn.h

#### uninstall base tensorflow and reinstall it with cuda ####
COPY configure-cuda /tensorflow/configure-cuda
COPY third_party/gpus/cuda/cuda_config.sh /tensorflow/third_party/gpus/cuda/cuda_config.sh
RUN pip uninstall -y tensorflow
RUN cd /tensorflow \
&& ./configure-cuda \
&& bazel build -c opt --config=cuda //tensorflow/tools/pip_package:build_pip_package \
&& bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg \
&& pip install /tmp/tensorflow_pkg/tensorflow-0.8.0-py2-none-any.whl
#### add some symbolic links because tensorflow is dumb and insists on looking for cuda 7.0 so's  ####
RUN ln -s /usr/local/cuda/lib64/libcublas.so.7.5 /usr/local/cuda/lib64/libcublas.so.7.0
RUN ln -s /usr/local/cuda/lib64/libcudart.so.7.5 /usr/local/cuda/lib64/libcudart.so.7.0
RUN ln -s /usr/local/cuda/lib64/libcufft.so.7.5 /usr/local/cuda/lib64/libcufft.so.7.0
RUN ln -s /usr/local/cuda/lib64/libcurand.so.7.5 /usr/local/cuda/lib64/libcurand.so.7.0

#### Everything past this point is optional ####
RUN pip install pygraphics \
    && pip install ampy \
    && apt-get install -y libopencv-dev python-opencv \
    && apt-get install -y --force-yes python-imaging-tk python-numpy python-pygame
RUN sudo apt-get install -y python-scipy
RUN pip install scikit-cuda
RUN pip install sklearn
RUN pip install yadlt
COPY third_party/py/Deep-Learning-TensorFlow/yadlt/core/model.py /usr/local/lib/python2.7/dist-packages/yadlt/core/model.py 
